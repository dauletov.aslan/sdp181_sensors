package kz.astana.senconapp;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.AppCompatActivity;

public class AccelerometerActivity extends AppCompatActivity {

    private TextView textView;
    private SensorManager sensorManager;
    private Sensor sensorAccelerometer;
    private Sensor sensorLinearAccelerometer;
    private Sensor sensorGravity;

    private StringBuilder stringBuilder;
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer);

        textView = findViewById(R.id.textView);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorLinearAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorGravity = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        stringBuilder = new StringBuilder();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(listener, sensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(listener, sensorLinearAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(listener, sensorGravity, SensorManager.SENSOR_DELAY_NORMAL);

        timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showInfo();
                    }
                });
            }
        };
        timer.schedule(task, 0, 400);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(listener);
        timer.cancel();
    }

    private String format(float values[]) {
        return String.format("%1$.1f\t\t%2$.1f\t\t%3$.1f", values[0], values[1], values[2]);
    }

    private float[] valuesAccelerometer = new float[3];
    private float[] valuesAccelerometerMotion = new float[3];
    private float[] valuesAccelerometerGravity = new float[3];
    private float[] valuesLinearAccelerometer = new float[3];
    private float[] valuesGravity = new float[3];

    private void showInfo() {
        stringBuilder.setLength(0);
        stringBuilder.append("Accelerometer: " + format(valuesAccelerometer))
                .append("\nAccelerometer motion: " + format(valuesAccelerometerMotion))
                .append("\nAccelerometer gravity: " + format(valuesAccelerometerGravity))
                .append("\nLinear accelerometer: " + format(valuesLinearAccelerometer))
                .append("\nGravity: " + format(valuesGravity));
        textView.setText(stringBuilder.toString());
    }

    private SensorEventListener listener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    for (int i = 0; i < 3; i++) {
                        valuesAccelerometer[i] = event.values[i];
                        valuesAccelerometerGravity[i] = (float) (0.1 * event.values[i] + 0.9 * valuesAccelerometerGravity[i]);
                        valuesAccelerometerMotion[i] = event.values[i] - valuesAccelerometerGravity[i];
                    }
                    break;
                case Sensor.TYPE_LINEAR_ACCELERATION:
                    for (int i = 0; i < 3; i++) {
                        valuesLinearAccelerometer[i] = event.values[i];
                    }
                    break;
                case Sensor.TYPE_GRAVITY:
                    for (int i = 0; i < 3; i++) {
                        valuesGravity[i] = event.values[i];
                    }
                    break;
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    public void openActivity(View view) {
        startActivity(new Intent(AccelerometerActivity.this, MagneticActivity.class));
    }
}